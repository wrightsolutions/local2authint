#!/bin/sh

LUSER=${USER}
if [ ! -z ${SUDO_USER} ]; then
	LUSER=${SUDO_USER}
fi

printf "/bin/groups lookup %s starting ...\n" "$LUSER"
/bin/groups $LUSER 2>&1 | \
python -c $'from __future__ import print_function;from sys import exit,stdin;import re;lines=stdin.readlines()\ncount = 0;matlines=[];re_unfound=re.compile(r"annot find name for group ID [0-9]*");\nmlist = re_unfound.findall("".join(lines));print(*mlist,sep=chr(10));exit(len(mlist))\n'
RC_=$?
printf "/bin/groups lookup for yourself returned and complete.\n"
exit $RC_
