#!/usr/bin/env python
# -*- coding: utf-8 -*-


# python ./slurmextrausers.py zzpro101 # in ID management probably anyhpc_zzpro101
# For much feedback on error before running execute export PYVERBOSITY=2

#from __future__ import with_statement
from datetime import datetime as dt
from os import chmod
from os import getenv as osgetenv
from os import path as ospath
#from os import stat as osstat
from os import sep
from sys import argv,exit
#from time import time
#import logging
import grp
from string import punctuation
from subprocess import Popen,PIPE
import shlex
import re


__version__ = '0.1'

program_basename = ospath.basename(argv[0])
progname, progext = ospath.splitext(program_basename)

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)
BUFSIZE1=4096
TABBY=chr(9)
UNDERSCORE=chr(95)
COLON=chr(58)
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)
DQ=chr(34)
SQ=chr(39)
PIPEY=chr(124)
CHMOD_UNPREFIXED=2770
CHMOD2=02770
#CHMOD3=0o2770

RE_NUMERIC_MARKER = re.compile('\d')

RE_ALPHA_UNDERSCORE = re.compile('[a-zA-Z]+{0}'.format(UNDERSCORE))
RE_ALPHANUM3 = re.compile('[a-zA-Z0-9]+')
RE_ALPHANUM4 = re.compile('[_a-zA-Z0-9]+')
RE_ALPHANUM3ACCOUNT = re.compile('account=[a-zA-Z0-9]+\s?$')
#RE_USERS_PID = re.compile(r"users{0}\{1}\{1}{2}.*{2},pid=".format(COLON,PARENTH_OPEN,DQ))
#RE_USERS_CSV = re.compile(r"users{0}{1}{1}<>{2}{2}".format(COLON,PARENTH_OPEN,PARENTH_CLOSE))
#RE_HPCN = re.compile('|lettuce|n')
RE_HPCN = re.compile('|lettuc||')
RE_ROOT_PIPEY = re.compile("{0}root{0}".format(PIPEY))
RE_HPCN_PIPEY = re.compile("{0}lettuce{0}".format(PIPEY))
RE_HPCN_PIPEY2 = re.compile("\{0}lettuce\{0}\{0}".format(PIPEY))

EQUALSREPEATED='='*3
GROUP_TEMPLATE="letthpc_{0}"

CMD_MKDIR='/usr/bin/mkdir'
CMD_CHGRP='/usr/bin/chgrp'
CHGRP_DEREF='--no-dereference'
CMD_RMDIR='/usr/bin/rmdir'
CMD_SACCTMGR_USERSFULL_NP_SORTED="/bin/sacctmgr -np show users withassoc | /usr/bin/sort -t'|' -k3"
CMD_SACCTMGR_USERSFULL_NP='/bin/sacctmgr -np show users withassoc'
CMD_SACCTMGR='/bin/sacctmgr'
SACCTMGR_USERSFULL_NP='-np show users withassoc'
SACCTMGR_ACCOUNTFULL_NP='-np show account withassoc'
SACCTMGR_ACCOUNTFULL_NP_TEMPLATE="-np show account withassoc account={0}"
"""/bin/sacctmgr -np show account account=rbusida withassoc
rbusida|rbusida|lettuce|lettuce_cluster|lettuce||1||||||||||||||ncl_qos|ncl_qos|
rbusida|rbusida|lettuce|lettuce_cluster||miss2|1||||||||||||||ncl_qos|ncl_qos|
rbusida|rbusida|lettuce|lettuce_cluster||mr48|1||||||||||||||ncl_qos|ncl_qos|
Account|Descr|Org|Cluster|ParName|User|Share|...|QOS|DefQOS|
"""
LOGGING_FULLPATH="/var/local/{0}.log".format(progname)

STRING_SUBOUT_ARGS_INVALID='Args list for subout is not valid - does it contain pipe (|)?'
STRING_NTH_ARG_REGEX_FAILED_TEMPLATE="Arg {0} did not pass the required regex test!"
STRING_NONZERO_RC_TEMPLATE="Command {0} gave non-zero returncode rc={1}"

gdict0 = {}
""" Above gdict0 is the group (account) dict from the scheduler query
Below gdict1 is the group dict from the identity management callout. """
gdict1 = {}
# Next dictionary is where we put the details of what *should* be added to Slurm
gdict0pending = {}

lines_feedback = []


def joined_from_list(arglist,joinstr=' '):
	return joinstr.join([str(arg) for arg in arglist])


def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not osstat(target_path).st_size:
        return False
    return True


def dirname_split_lower(dirname,lowcase=True):
	dsplit = dirname.split('_')[-1]
	if len(dsplit) > 0:
		dres = str.lower(dsplit)
	else:
		dres = dsplit
	return dres


def args_valid(args_list):
	pipe_count = 0
	for arg in args_list:
		try:
			if PIPEY in arg:
				pipe_count+=1
				break
		except TypeError:
			pass

	return (pipe_count < 1)


def args_valid3(args=[], argnum=0, arg_regex=None):
	feedback_line = ''
	arg=args[argnum]
	#print("Testing arg={0}".format(arg))
	valid3rc = 0
	if arg_regex.match(arg):
		pass
	else:
		# 1+argnum so report back to the user is more natural (one indexed)
		feedback_line = STRING_NTH_ARG_REGEX_FAILED_TEMPLATE.format(1+argnum)
		valid3rc = 204
	return (valid3rc,feedback_line)


def dict0accountfull(group_unprefixed,print_flag=True):
	""" Populate group dictionary by querying scheduler (Slurm) """
	global gdict0
	group_prefixed = None
	dict_filtered = {}
	sacctmgr_argslist = shlex.split(SACCTMGR_ACCOUNTFULL_NP)
	if group_unprefixed is not None:
	        sacctmgr_argslist = shlex.split(SACCTMGR_ACCOUNTFULL_NP_TEMPLATE.format(group_unprefixed))
		arg_regex_rc,feedback_line = args_valid3([group_unprefixed], 0, RE_ALPHANUM3)
		if arg_regex_rc > 0:
			sub_rc = arg_regex_rc
			lines_feedback.append(feedback_line)
		#, , 4, RE_ALPHANUM3ACCOUNT
	""" Fetch from Slurm because we have something meaningful from grp callout
	to compare against.
	"""
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		print(sacctmgr_argslist)
		sub_rc, lines_out, lines_err, lines_feedback, _ = \
			subout4match_wrapper(CMD_SACCTMGR, sacctmgr_argslist, 0, None)
	else:
		# Enable silent running by setting PYVERBOSITY=0 in your environment
		sub_rc, lines_out, lines_err, lines_feedback, _ = \
			subout2match(CMD_SACCTMGR, sacctmgr_argslist)
	"""
	if PYVERBOSITY > 1 and len(lines_feedback) > 0:
		for line in lines_feedback:
			print(line)
	"""
	if PYVERBOSITY > 1 and len(lines_out) > 0:
		for line in lines_out:
			print(line)
	print(EQUALSREPEATED)

	print(len(lines_out),len(lines_out))
	lines_account = []
	for line in lines_out:
		#if RE_ROOT_PIPEY.match(line):
		#	continue
		if RE_HPCN_PIPEY2.search(line):
			print("found it {1} prefixed {2}".format(None,
				line[RE_HPCN_PIPEY2.search(line).start():],
				line[:RE_HPCN_PIPEY2.search(line).start()]))
			continue
		lines_account.append(line)

	account_stored = None
	lines_filtered = []
	users = []
	#print(len(lines_account),len(lines_account))
	for line in sorted(lines_account):
		#print(line)
		line_array = line.split(PIPEY)
		account = line_array[0]
		parname = line_array[4]
		if len(parname) >= 1:
			print("parname={0} found so skipping this line".format(parname))
			continue
		if group_unprefixed is not None:
			if PYVERBOSITY is None or PYVERBOSITY > 1:
				print("equality={1} for account={2} {3}".format(None,
							(account == group_unprefixed),
							account,line))
			if account != group_unprefixed:
				# Not an account we are interested in so no more proc
				continue
		lines_filtered.append(line)
		user = line_array[5]
		print(user,user,user,user,user,user)
		dict_filtered[account] = user
		if account != account_stored:
			if account_stored is None:
				account_stored = account
				users = []
			else:
				gdict0[account_stored] = users
				if print_flag is True:
					print("gdict0['{0}'] set to {1}".format(account_stored,users))
				account_stored = account
				users = []
		users.append(user)
		if print_flag is not True:
			continue
		if PYVERBOSITY is None or PYVERBOSITY > 1:
			print("user={0} should now be in this ... {1}".format(user,
							dict_filtered[account]))
		# end of loop over lines_account

	if len(users) > 0:
		if PYVERBOSITY is None or PYVERBOSITY > 1:
			print("users={0} is hanging after loop close".format(users))
			print("len(users)={1} hanging after loop account={2}".format(None,
										users,
										account))
		gdict0[account] = users
		# Above we deal with a loop closed but action pending type situation
		
	if PYVERBOSITY > 1 and len(lines_filtered) > 0:
		for lines in lines_filtered:
			print(line.split(PIPEY)[4],line)

	return dict_filtered


def dict1groupmembership(group_given,prefix_flag=False):
	dict1grp_rc = 0
	group_prefixed = group_given
	if prefix_flag is True:
		group_prefixed = GROUP_TEMPLATE.format(group_given)
	if PYVERBOSITY is None or PYVERBOSITY > 1:
 		print("Formed group1prefixed {0} from {1}".format(group_prefixed,
								group_given))
 	gr_mem_list = ['unfetched!']
 	try:
 		gr_mem_list = grp.getgrnam(group_prefixed).gr_mem
 	except KeyError:
 		gr_mem_list = ["fetchfail!_for_group:{0}".format(group_prefixed)]
 		dict1grp_rc = 211

	len1 = len(gr_mem_list)

	if 0 == dict1grp_rc:
		gdict1[group_prefixed] = gr_mem_list

	line1 = "1group_prefixed={0} members={1}".format(group_prefixed,gr_mem_list)

	try:
		logf.write("{0} {1}\n".format(dt.now().isoformat(),line1))
		#printf(line0)
		logf
	except:
		pass

	return gr_mem_list


def gdict0gdict1compare(print_flag=False,g0missing_flag=False,logfile=None):
	global gdict0pending
	addcount = 0
	for g1,g1mem in sorted(gdict1.items()):
		garray = g1.split(UNDERSCORE)
		g0lookup = garray[1]
		if g0lookup in gdict0:
			# Group exists in Slurm
			mem0set = set(gdict0[g0lookup])
			mem1set = set(gdict1[g1])
			# addition
			g0additions = mem1set - mem0set
			if len(g0additions) < 1:
				continue
			addcount+=len(g0additions)
			g0addlist = sorted(list(g0additions))
			if logfile is None:
				continue
			line3 = "3account={0} additions0={1}".format(g0lookup,g0addlist)
			try:
				logf.write("{0} {1}\n".format(dt.now().isoformat(),line3))
				#printf(line0)
				logf
			except:
				pass
		else:
			# Group would be NEW to Slurm
			if g0missing_flag is not True:
				continue
			# Log [and print] something about missing g0 group
			if logfile is None:
				continue
			line3 = "3accountMISSING={0} !!!".format(g0lookup)
			try:
				logf.write("{0} {1}\n".format(dt.now().isoformat(),line3))
				#printf(line0)
				logf
			except:
				pass
		
	return addcount


def remove_dir_on_request(dir2pathed):
	# Cleanup as only partially successful
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		print("remove_dir_on_request() called for {0}".format(dir2pathed))
	rmdir_argslist = [dir2pathed]
	_, lines_out, lines_err, lines_feedback, _ = \
			subout4match(CMD_RMDIR, rmdir_argslist, 0, None)
	return lines_feedback


def subout4(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	s_rc = 0
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	for line in cmdproc.stdout:
		print(line)

	"""
	subout = (Popen(shlex.split(findcmd),bufsize=4096,stdout=PIPE)).stdout
	subout = (Popen(findcmd.split(' '),bufsize=4096,stdout=PIPE)).stdout
	"""
	return s_rc


def subout2match(command_without_args, args=[]):
	""" Suffix of 2 as we accept two args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	lines_feedback = []
	command_with_args = None
	s_rc = 0
	if args_valid(args):
		pass
	else:
		lines_feedback = [STRING_SUBOUT_ARGS_INVALID]
		s_rc = 202

	if s_rc > 0:
		return (s_rc,[],[],lines_feedback,command_with_args)

	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	if PYVERBOSITY > 1:
		print(command_with_args)
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = []
	for line in cmdproc.stdout:
		lines.append(line)
		#print(line)

	cmdproc.wait()
	s_rc = cmdproc.returncode
	if s_rc > 0:
		feedback_rc = STRING_NONZERO_RC_TEMPLATE.format(command_without_args,s_rc)
		lines_feedback.append(feedback_rc)
		if 1==s_rc and CMD_SACCTMGR==command_without_args:
			lines_feedback.append('Did you try to chgrp to an empty group?')
	#print(cmdproc.returncode)
	return (s_rc,lines,[],lines_feedback,command_with_args)


def subout4match(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)

	Giving third and fourth args as 0, None makes this a near analog
	of subout2match() as third and fourth args are effectively ignored
	"""
	lines_feedback = []
	command_with_args = None
	s_rc = 0
	if args_valid(args):
		pass
	else:
		lines_feedback = [STRING_SUBOUT_ARGS_INVALID]
		s_rc = 202

	if arg_regex is not None:
		arg_regex_rc,feedback_line = args_valid3(args, argnum, arg_regex)
		if arg_regex_rc > 0:
			s_rc = arg_regex_rc
			lines_feedback.append(feedback_line)

	if s_rc > 0:
		return (s_rc,[],[],lines_feedback,command_with_args)

	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	if PYVERBOSITY > 1:
		print(command_with_args)
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = []
	for line in cmdproc.stdout:
		lines.append(line)
		#print(line)

	cmdproc.wait()
	s_rc = cmdproc.returncode
	if s_rc > 0:
		feedback_rc = STRING_NONZERO_RC_TEMPLATE.format(command_without_args,s_rc)
		lines_feedback.append(feedback_rc)
		if 1==s_rc and CMD_SACCTMGR==command_without_args:
			lines_feedback.append('Did you try to chgrp to an empty group?')
	#print(cmdproc.returncode)
	return (s_rc,lines,[],lines_feedback,command_with_args)


def subout4match_wrapper(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Wrapper that will print lines_feedback when return code > 0 """
	s_rc, lines_out, lines_err, lines_feedback, command_with_args = \
		subout4match(command_without_args, args, argnum, arg_regex)
	if s_rc > 0:
		for line in lines_feedback:
			print(line)

	return (s_rc,lines_out,lines_err,lines_feedback,command_with_args)
	

def subout4splitn(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	s_rc = 0
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = cmdproc.stdout.split('\n')
	for line in lines:
		print(line)

	return s_rc


if __name__ == '__main__':

	exit_rc = 0

	group_unprefixed_supplied = None   
	if len(argv) > 1:
		group_unprefixed_supplied = argv[1]
		#if len(argv) > 2:
                #else:
                #    exit_rc = 102
        else:
		pass
		#exit_rc = 101

	if exit_rc > 0:
		exit(exit_rc)

	sub_rc = 0

	#subout4splitn_match(command_without_args, args=[], argnum=0, arg_regex=None)

	dict0 = dict0accountfull(group_unprefixed_supplied,True)
	list0 = dict0.keys()
	print("list0 is {0}".format(list0))
#	dict0accountfull(group_unprefixed_supplied,True)

	logf = None
	try:
		logf = open(LOGGING_FULLPATH,'a')
	except:
		logf = None
		# If we cannot log then issue verbose output
		printf("open of logf={0} failed.")
		sub_rc = 221

	""" Next we use gdict0 rather than dict0 as we want
	a FULL members list """
	for acc,mlist in gdict0.items():
		line0 = "0account_slurm={0} members={1}".format(acc,mlist)
		try:
			logf.write("{0} {1}\n".format(dt.now().isoformat(),line0))
			#printf(line0)
		except:
			sub_rc = 231
			break

	if sub_rc > 0:
		exit(sub_rc)

	""" Now that Slurm lookup side of things completed, we turn our attention to AD
	"""

	count1iterated = 0
	count1populated = 0
	if group_unprefixed_supplied is None:
		print("dict0 is {0}".format(dict0))
		for group1 in list0:
			count1iterated+=1
			group1list = dict1groupmembership(group1,True)
			if len(group1list) > 0:
				# Have a populated group so increment count
				count1populated+=1
		if count1iterated > count1populated:
			sub_rc = 231

	else:
		group1list = dict1groupmembership(group_unprefixed_supplied,True)
		if len(group1list) < 1:
			sub_rc = 211
	# gdict1 (members lists from AD) should now be populated and ready for compares

	if PYVERBOSITY > 1:
		print(gdict0.keys())
		for k,v in gdict0.items(): print(k," ",v)

	if sub_rc > 0:
		exit(sub_rc)


	additions_count = 0
	additions_count = gdict0gdict1compare(True,True,logf)

	if logf is not None:
		logf.close()

	if sub_rc > 0:
		exit(sub_rc)

	exit(sub_rc)

