#!/usr/bin/env python
#
#  Copyright 2017,2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

""" Extract summary information about a running process and store the samples
in an sqlite db.
First parameter is the pid of the running process you want to get information about
Second (optional) parameter is the directory to which we should log

Example of how to monitor the running process with pid 1808
  python ./every2fields5ctx.py 1808 /var/log

When the process you are monitoring is terminated, then this Python process will
also terminate on the next sample attempt.
"""

from __future__ import print_function
from __future__ import with_statement

from os import path,sep
from sys import argv
#import logging
from datetime import date
from datetime import datetime as dt
import re
import sqlite3 as db3
from sys import argv
from time import sleep

from string import ascii_letters,digits,printable
#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE = set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))

try:
    import psutil
except ImportError:
    print("Is python-psutil (Psutil) installed?")
    #print("Is python-psutil (Psutil) installed? Import error {0}".format(e))
    # apt-get install python-psutil
    exit(191)

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'
MEG_AS_FLOAT = float(2**20)
SQLSTRING_CREATE_PROCMEM = ("CREATE TABLE IF NOT EXISTS"
" procmem(pid integer NOT NULL,"
" cmdword1 text NOT NULL, "
" cmdword2 text NOT NULL, "
" nixtime_created real NOT NULL,"
" sampled_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
" mrss real NOT NULL,"
" mvms real NOT NULL,"
" mshared real NOT NULL,"
" mtext real NOT NULL,"
" mdata real NOT NULL,"
" isotimeutc_created text,"
" cmdline text,"
" threads real,"
" switches real,"
" swinvol real,"
" affinity text,"
" PRIMARY KEY(pid,cmdword1,cmdword2,sampled_at)"
" );")
SLEEPSECS = 120
TWO_TO_TWENTYTWO = 2**22

dbcon = None

dbfound_flag = False
tablefound_flag = False
date_today = date.today()
isoformat_now = dt.now().isoformat()
yyyyww = "{0}{1:0>2}".format(date_today.year,date_today.isocalendar()[1])


def connect_get_cursor(database_path,debug=False):
    global dbcon
    cur = None
    isoformat_now = dt.now().isoformat()
    if path.exists(database_path):
        print("{0} Attempting append to sqlite db at {1}.".format(
                isoformat_now,database_path))
        try:
            dbcon = db3.connect(database_path,timeout=2)
        except db3.Error as e:
            print('Sqlite connection error occurred: %s', e.args[0])
            return None
        except Exception as e:
            print('Sqlite connection error: %s', e)
            return None
	dbcon.isolation_level = None
	cur = dbcon.cursor()
    else:
	dbcon = db3.connect(database_path)
        print("{0} Creating initial sqlite db at {1}.".format(
                isoformat_now,database_path))
        #print(SQLSTRING_CREATE_PROCMEM)
	dbcon.execute(SQLSTRING_CREATE_PROCMEM)
	cur = dbcon.cursor()
    return cur


def create_table(cur):
    """ Having a cursor object does not guarantee that the file was an sqlite
    database. A plain text file get a cursor, but fails on cur.execute() """
    global tablefound_flag

    #if debug is True:
    #    isoformat_now = dt.now().isoformat()
    #    print("{0} Creating target table in the sqlite db.".format(isoformat_now))

    try:
        #print(SQLSTRING_CREATE_PROCMEM)
        cur.execute(SQLSTRING_CREATE_PROCMEM)
        tablefound_flag = True
    except db3.ProgrammingError as e:
        err_str = "Sqlite execute() ProgrammingError occurred: {0}".format(e)
        print(err_str)
        cur.close()
        return False
    except db3.Error as e:
        err_str = "Sqlite execute() create table error {0}: {1}".format(
            database_path, e.args[0])
        print(err_str)
        cur.close()
        return False
    except:
        err_str = "Sqlite error during .execute() create if not exists."
        print(err_str)
        cur.close()
        return False
    return True


def insert_sample(sample_tuple,cur=None):

    global tablefound_flag

    if not tablefound_flag:
        create_table(dbcon.cursor())

    #print("Next we insert into the procmem table.")
    try:
        if cur is None:
            curinsert = dbcon.cursor()
        else:
            curinsert = cur
        #print(sample_tuple[1])
        curinsert.execute("""INSERT INTO procmem (pid,cmdword1,cmdword2,nixtime_created,
mrss,mvms,mshared,mtext,mdata,isotimeutc_created,
cmdline,threads,switches,swinvol,affinity) 
VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);
""",sample_tuple)
    except db3.OperationalError as e:
        err_str = 'Sqlite execute() OperationalError occurred: {0}'.format(e)
        print(err_str)
        if cur is None:
            curinsert.close()
        return False
    except db3.ProgrammingError as e:
        err_str = 'Sqlite execute() ProgrammingError occurred: {0}.format(e)'
        print(err_str)
        if cur is None:
            curinsert.close()
        return False
    except db3.Error as e:
        err_str = 'Sqlite execute() error occurred: {0}'.format(e)
        print(err_str)
        if cur is None:
            curinsert.close()
        curinsert.close()
        return False
    except ValueError as e:
        err_str = 'Sqlite insert failed ValueError: {0}'.format(e)
        print(err_str)
        if cur:
            curinsert.close()
        curinsert.close()
        return False
    except Exception as e:
        err_str = 'Sqlite insert failed: {0}'.format(e)
        print(err_str)
        if cur:
            curinsert.close()
        curinsert.close()
        return False

    if cur is None:
        curinsert.close()
    return True


def report_survived(pid_given,survived3state):
    isocompact = dt.now().strftime(ISOCOMPACT_STRFTIME)
    print("{0} Process with pid {1} ".format(isocompact,pid_given),end='')
    if survived3state is None:
        print("had no effective monitoring!".format(pid_given))
    elif survived3state is True:
        print("survived all intensive monitoring.".format(pid_given))
    else:
        print("errored or was stopped during monitoring.".format(pid_given))
    return


def looped_sampling2(pid_given,cur=None,dbpath=None):
    """ 1440 minutes in a day. 720 iterations per day if you are sleeping
    for 2 minutes. Setting a 100 day limit for sleep(60) would be 100*1440
    but setting a 100 day limit for sleep(120) would be 100*720 which is 72000
    Why 100 days? If you have a memory leak and it is not found in the first 100
    days then you probably need a more thorough investigation than can be
    supported by this tool.

    Returns true if every iteration of the while loop completed
    We consider then that the process has survived all intensive monitoring,
    and set process_survived to True.

    This version of looped_sampling designed for version 2 of psutil method calls.
    """

    global database_path

    idx_upper = 100*720

    process3state = None

    if dbpath is None:
        return process3state

    idx = 0
    sample_tuple = ()
    while idx < idx_upper:
        try:
            psample = psutil.Process(pid_given)
            nixtime_created = psample.create_time()
            isotimeutc_created = dt.isoformat(dt.utcfromtimestamp(nixtime_created))
            pmem_tuple = psample.get_ext_memory_info()
            mrss = pmem_tuple.rss / MEG_AS_FLOAT
            mvms = pmem_tuple.vms / MEG_AS_FLOAT
            mshared = pmem_tuple.shared / MEG_AS_FLOAT
            mtext = pmem_tuple.text / MEG_AS_FLOAT
            mdata = pmem_tuple.data / MEG_AS_FLOAT
            cmdline_list = psample.cmdline()
            cmdline = chr(32).join(cmdline_list)
            cmdword1 = psample.name()
            cmdword2 = cmdline_list[0]
            threads = psample.get_num_threads()
            sw = psample.get_num_ctx_switches()
            switches = sw.voluntary
            swinvol = sw.involuntary
            affinity_list = psample.get_cpu_affinity()
            affinity_strings = [ str(aff) for aff in affinity_list ]
            affinity = chr(32).join(affinity_strings)
            #print(switches,swinvol,affinity)

        except AttributeError:
            print("AttributeError - is the correct version of python-psutil installed?")
            break
        except psutil.NoSuchProcess:
            """ The process has not survived all iterations of the while loop """
            break
            #insert_sample(sample_tuple,cur=None)

        sample_tuple = (pid_given,cmdword1,cmdword2,nixtime_created,
                        mrss,mvms,mshared,mtext,mdata,isotimeutc_created,
                        cmdline,threads,switches,swinvol,affinity)

        insert_sample(sample_tuple,cur)

        process3state = False
        #print(sample_tuple)
        idx += 1
        try:
            cur.execute("commit;")
        except:
            pass
        sleep(SLEEPSECS)
        # Sleep is the last thing we do in this loop iteration
    else:
        # We completed all iterations - the process survived
        process3state = True

    return process3state


def looped_sampling_all(pid_given,cur=None,dbpath=None):
    """ 1440 minutes in a day. 720 iterations per day if you are sleeping
    for 2 minutes. Setting a 100 day limit for sleep(60) would be 100*1440
    but setting a 100 day limit for sleep(120) would be 100*720 which is 72000
    Why 100 days? If you have a memory leak and it is not found in the first 100
    days then you probably need a more thorough investigation than can be
    supported by this tool.

    Returns true if every iteration of the while loop completed
    We consider then that the process has survived all intensive monitoring,
    and set process_survived to True.
    """

    global database_path

    idx_upper = 100*720

    process3state = None

    if dbpath is None:
        return process3state

    if pid_given > TWO_TO_TWENTYTWO:
        return process3state

    idx = 0
    sample_tuple = ()
    while idx < idx_upper:
        try:
            psample = psutil.Process(pid_given)
            nixtime_created = psample.create_time()
            isotimeutc_created = dt.isoformat(dt.utcfromtimestamp(nixtime_created))
            pmem_tuple = psample.get_ext_memory_info()
            mrss = pmem_tuple.rss / MEG_AS_FLOAT
            mvms = pmem_tuple.vms / MEG_AS_FLOAT
            mshared = pmem_tuple.shared / MEG_AS_FLOAT
            mtext = pmem_tuple.text / MEG_AS_FLOAT
            mdata = pmem_tuple.data / MEG_AS_FLOAT
            cmdline_list = psample.cmdline()
            cmdline = chr(32).join(cmdline_list)
            cmdword1 = psample.name()
            cmdword2 = cmdline_list[0]
            threads = psample.get_num_threads()
            sw = psample.get_num_ctx_switches()
            switches = sw.voluntary
            swinvol = sw.involuntary
            affinity_list = psample.get_cpu_affinity()
            affinity_strings = [ str(aff) for aff in affinity_list ]
            affinity = chr(32).join(affinity_strings)
            #print(switches,swinvol,affinity)

        except AttributeError:
            print("AttributeError - is the correct version of python-psutil installed?")
            break
        except psutil.NoSuchProcess:
            """ The process has not survived all iterations of the while loop """
            break
            #insert_sample(sample_tuple,cur=None)

        sample_tuple = (pid_given,cmdword1,cmdword2,nixtime_created,
                        mrss,mvms,mshared,mtext,mdata,isotimeutc_created,
                        cmdline,threads,switches,swinvol,affinity)

        insert_sample(sample_tuple,cur)

        process3state = False
        print(sample_tuple)
        idx += 1
        try:
            cur.execute("commit;")
        except:
            pass
        sleep(SLEEPSECS)
        # Sleep is the last thing we do in this loop iteration
    else:
        # We completed all iterations - the process survived
        process3state = True

    return process3state


if __name__ == "__main__":

    pid_given = None
    pathstem_given = None
    pathstem_expanded = path.expanduser('~')

    DEBUG = False

    if (len(argv) < 2):
        exit(131)

    if len(argv) > 1:
        pid_string = argv[1]

        if len(argv) > 2:
            pathstem = argv[2]
            if pathstem.startswith('/var/') or pathstem.startswith('/opt/'):
                pathstem_given = pathstem

    if pid_string == 'ALL' or pid_string == '*':
        pid_given = 'ALL'
    elif set(pid_string).issubset(SET_LOWERUPPER_AND_DIGITS):
        try:
            pid_given = int(pid_string)
        except:
            exit(136)
    else:
        # We will NOT use what is given to generate a filename
        exit(136)

    if pathstem_given is None:
        if DEBUG is True:
            print(".sqlite file will be created in {0} by default.".format(pathstem_expanded))
        database_path = '{0}{1:<1}psleeper{2}fields5ctx__{3}.sqlite'.format(
            pathstem_expanded,sep,pid_given,yyyyww)
    else:
        if DEBUG is True:
            print(".sqlite file will be created in {0} as requested.".format(pathstem_given))
        database_path = '{0}{1:<1}psleeper{2}fields5ctx__{3}.sqlite'.format(
            pathstem_given,sep,pid_given,yyyyww)

    if DEBUG is True:
        print("{0} any database logging will go to {1}".format(isoformat_now,database_path))

    survived3state = None
    try:
        cur = connect_get_cursor(database_path,DEBUG)
        if pid_given > 0 and pid_given < TWO_TO_TWENTYTWO:
            survived3state = looped_sampling2(pid_given,cur,database_path)
        else:
            survived3state = looped_sampling_all(pid_given,cur,database_path)
    except:
        report_survived(pid_given,survived3state)
        raise
        exit(141)

    report_survived(pid_given,survived3state)

    if dbcon:
        cur = dbcon.cursor()
        try:
            cur.execute("commit;")
        except:
            pass
        dbcon.close()

    if survived3state is None:
        exit(151)

