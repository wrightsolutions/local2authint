#/bin/sh
# Example commands for getting sqlitebrowser on a Centos 7 system
#
# Which Fedora is most compatible with Centos 7 ?
# Red Hat Enterprise Linux 7 (Maipo) is based on Fedora 19, released June 10 2014 based on upstream Linux kernel 3.10,
# ...and systemd 208, and GNOME 3.8 (rebased to GNOME 3.14 in RHEL 7.2)
# Key dates of Centos 7: Released July 2014 ; 
# Centos 7 Full updates until Q4 2020 ; Maint updates until 30 June 2024
#
yum install qt-devel
# Above will give you /usr/bin/qmake-qt4
# Untar  b5f2b25a61aa70bb95838014b801afb2df6105cf  sqlitebrowser-3.9.1.tar.gz
cd /opt/local/
tar -zxf sqlitebrowser-3.9.1.tar.gz
cd /opt/local/sqlitebrowser-3.9.1
make clean
/usr/bin/qmake-qt4
make
#
# Your binary should now live at src/sqlitebrowser
# On my system the following sha1sum
#  676caf692807a99b253b9194a716caa28d0a726e  sqlitebrowser
exit 0
# On my system the following ldd output:
#  	linux-vdso.so.1 =>  (0x00007ffc48ffd000)
#	libsqlite3.so.0 => /lib64/libsqlite3.so.0 (0x00007fc5cf64e000)
#	libdl.so.2 => /lib64/libdl.so.2 (0x00007fc5cf44a000)
#	libQtGui.so.4 => /lib64/libQtGui.so.4 (0x00007fc5ce76e000)
#	libQtNetwork.so.4 => /lib64/libQtNetwork.so.4 (0x00007fc5ce428000)
#	libQtCore.so.4 => /lib64/libQtCore.so.4 (0x00007fc5cdf3d000)
#	libpthread.so.0 => /lib64/libpthread.so.0 (0x00007fc5cdd20000)
#	libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007fc5cda17000)
#	libm.so.6 => /lib64/libm.so.6 (0x00007fc5cd715000)
#	libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007fc5cd4fe000)
#	libc.so.6 => /lib64/libc.so.6 (0x00007fc5cd13d000)
#	/lib64/ld-linux-x86-64.so.2 (0x00007fc5cf91b000)
#	libgthread-2.0.so.0 => /lib64/libgthread-2.0.so.0 (0x00007fc5ccf3b000)
#	libglib-2.0.so.0 => /lib64/libglib-2.0.so.0 (0x00007fc5ccc03000)
#	libpng15.so.15 => /lib64/libpng15.so.15 (0x00007fc5cc9d8000)
#	libz.so.1 => /lib64/libz.so.1 (0x00007fc5cc7c2000)
#	libfreetype.so.6 => /lib64/libfreetype.so.6 (0x00007fc5cc51b000)
#	libgobject-2.0.so.0 => /lib64/libgobject-2.0.so.0 (0x00007fc5cc2cb000)
#	libSM.so.6 => /lib64/libSM.so.6 (0x00007fc5cc0c3000)
#	libICE.so.6 => /lib64/libICE.so.6 (0x00007fc5cbea6000)
#	libXi.so.6 => /lib64/libXi.so.6 (0x00007fc5cbc96000)
#	libXrender.so.1 => /lib64/libXrender.so.1 (0x00007fc5cba8c000)
#	libXrandr.so.2 => /lib64/libXrandr.so.2 (0x00007fc5cb881000)
#	libXfixes.so.3 => /lib64/libXfixes.so.3 (0x00007fc5cb67b000)
#	libXcursor.so.1 => /lib64/libXcursor.so.1 (0x00007fc5cb470000)
#	libXinerama.so.1 => /lib64/libXinerama.so.1 (0x00007fc5cb26c000)
#	libfontconfig.so.1 => /lib64/libfontconfig.so.1 (0x00007fc5cb02f000)
#	libXext.so.6 => /lib64/libXext.so.6 (0x00007fc5cae1d000)
#	libX11.so.6 => /lib64/libX11.so.6 (0x00007fc5caade000)
#	libssl.so.10 => /lib64/libssl.so.10 (0x00007fc5ca870000)
#	libcrypto.so.10 => /lib64/libcrypto.so.10 (0x00007fc5ca486000)
#	librt.so.1 => /lib64/librt.so.1 (0x00007fc5ca27d000)
#	libffi.so.6 => /lib64/libffi.so.6 (0x00007fc5ca075000)
#	libuuid.so.1 => /lib64/libuuid.so.1 (0x00007fc5c9e6f000)
#	libexpat.so.1 => /lib64/libexpat.so.1 (0x00007fc5c9c45000)
#	libxcb.so.1 => /lib64/libxcb.so.1 (0x00007fc5c9a22000)
#	libgssapi_krb5.so.2 => /lib64/libgssapi_krb5.so.2 (0x00007fc5c97d4000)
#	libkrb5.so.3 => /lib64/libkrb5.so.3 (0x00007fc5c94ed000)
#	libcom_err.so.2 => /lib64/libcom_err.so.2 (0x00007fc5c92e8000)
#	libk5crypto.so.3 => /lib64/libk5crypto.so.3 (0x00007fc5c90b6000)
#	libXau.so.6 => /lib64/libXau.so.6 (0x00007fc5c8eb2000)
#	libkrb5support.so.0 => /lib64/libkrb5support.so.0 (0x00007fc5c8ca2000)
#	libkeyutils.so.1 => /lib64/libkeyutils.so.1 (0x00007fc5c8a9e000)
#	libresolv.so.2 => /lib64/libresolv.so.2 (0x00007fc5c8884000)
#	libselinux.so.1 => /lib64/libselinux.so.1 (0x00007fc5c865c000)
#	libpcre.so.1 => /lib64/libpcre.so.1 (0x00007fc5c83fb000) 
#
exit 0
# Avoid cmake altogether as sqlitebrowser for Fedora / CentOS is only 'qmake' supported
sudo yum install qt-devel ant-antlr sqlite-devel antlr-C++
sudo yum install cmake
cmake .
[ $? -ne 0 ] && exit $?
make
[ $? -ne 0 ] && exit $?
sudo make install
[ $? -ne 0 ] && exit $?
exit

# Nearest fcNN for qhexedit2-qt5 is Fedora 22 so missing qhexedit2 depends might find there
# Spacing in directory names? Not something advisable for cmake support me thinks:
# CMakeFiles/DB Browser for SQLite.dir/build.make
# fgrep OBJ "CMakeFiles/DB Browser for SQLite.dir/build.make"
# DB Browser for SQLite_OBJECTS = \
# DB Browser for SQLite_EXTERNAL_OBJECTS =
#
